//
//  main.m
//  HomeTaskLessons
//
//  Created by Fantom on 2/5/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASPatient.h"
#import "ASDoctor.h"
#import "ASFriendOFDoctor.h"

int main() {
    @autoreleasepool {
        ASDoctor* doctor = [[ASDoctor alloc] initWithName:@"Good doctor"];
        ASFriendOFDoctor* badDoctor = [[ASFriendOFDoctor alloc] initWithName:@"Bad doctor"];
        ASFriendOFDoctor* horribleDoctor = [[ASFriendOFDoctor alloc] initWithName:@"Horrible doctor"];
        
        ASPatient* patient1 = [[ASPatient alloc] initWithName:@"Sasha" hasTemperature:37.7f hasDoctor:doctor];
        ASPatient* patient2 = [[ASPatient alloc] initWithName:@"Vitay" hasTemperature:36.9f hasDoctor:doctor];
        ASPatient* patient3 = [[ASPatient alloc] initWithName:@"Petia" hasTemperature:40.1f hasDoctor:doctor];
        ASPatient* patient4 = [[ASPatient alloc] initWithName:@"Ira" hasTemperature:38.f hasDoctor:doctor];
        
        
        NSArray* patients = @[patient1, patient2, patient3, patient4];
        NSArray* doctors = @[doctor, badDoctor, horribleDoctor];
        
        //level Begginer: put all patients to an array, and call their "feelBad" method
        for (ASPatient* patient in patients) {
            NSLog(@"%@ asks %@: Do you feel bad ? %@", [patient.delegate getName], patient.name, [patient feelBad] ? @"YES" : @"NO");
        }
        
        //level Student: adding a bad doctor
        patient3.delegate = badDoctor;
        patient4.delegate = horribleDoctor;
        
        for (ASPatient* patient in patients) {
            NSLog(@"%@ asks %@: Do you feel bad ? %@", [patient.delegate getName], patient.name, [patient feelBad] ? @"YES" : @"NO");
        }
        
        //level Master: adding raport method for all doctors.
        for (id doctor in doctors) {
            [doctor showAllPatientsOfTheDay];
        }
        
        //level Superman: adding the rating for all pations. some of them will be unsatiffied after they visit a doctor. So, for these patients, the doctor will be changed.
        for (ASPatient* patient in patients) {
            NSLog(@"%@ has doctor:%@", patient.name, [patient.delegate getName]);
        }
        
        for (ASPatient* patient in patients) {
            if (!patient.isSatisfied) {
                [patient changeDoctor:doctors];
            }
        }
        // making sure, that unsatisfied patients now have another doctor
        for (ASPatient* patient in patients) {
            NSLog(@"%@ has doctor:%@", patient.name, [patient.delegate getName]);
        }
        
    }
    
    return 0;
}