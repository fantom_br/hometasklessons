/*!
 @header ASFriendOFDoctor
 
 @brief This is the header file of my ASFriendOFDoctor class
 This class was created in order to accomplish home task of lesson9 Delegates, in IOS development Course in VK social network.
 
 @author Fantom
 @copyright  2015 Fantom
 @version    1.0
 */

#import <Foundation/Foundation.h>
#import "ASPatient.h"

/*!
 @class ASFriendOFDoctor
 
 @brief The ASFriendOFDoctor class
 
 @discussion This class is exactly the same as ASDoctor class. I didn't inherit it from ASDoctor due to requirements to the home task. To get full gescription please see ASDoctor description.
 @coclass: ASPatient and ASDoctor
 */
@interface ASFriendOFDoctor : NSObject <ASPatientDelegate>

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSMutableArray* patientsWithHeadAche;
@property (strong, nonatomic) NSMutableArray* patientsWithStomacAche;
@property (strong, nonatomic) NSMutableArray* patientsWithHeartAche;
@property (strong, nonatomic) NSMutableArray* patientsWithPainInLeg;

- (id) initWithName:(NSString*) name;
- (void) showAllPatientsOfTheDay;

@end
