/*!
 @header ASPatient.h
 
 @brief This is the header file of my ASPatient class
 This class was created in order to accomplish home task of lesson9 Delegates, in IOS development Course in VK social network.
 
 @author Fantom
 @copyright  2015 Fantom
 @version    1.0
 */

#import <Foundation/Foundation.h>

@class ASDoctor;
@protocol ASPatientDelegate;

/*!
 @class ASPatient
 
 @brief The ASPatient class
 
 @discussion This class is describing a typical patient, which is going to a doctor. He has properties like name, his temperature, other symptoms like cough. The Patient has one delegate which is ASDoctor. After each visit, patient will be either satisfied or not, and in case if not, the doctor will be changed for such patient
 @coclass: ASDOctor
 */
@interface ASPatient : NSObject

/*!@brief It describes a Name of Patient*/
@property (strong, nonatomic) NSString* name;
/*!@brief It shows a temperature of Patient*/
@property (assign, nonatomic) CGFloat temperature;
/*!@brief It's a delegate(ASDoctor) of Patient's class*/
@property (weak, nonatomic) id<ASPatientDelegate> delegate;
/*!@brief It shows whether Patient has cough or not*/
@property (assign, nonatomic) BOOL hasCough;
/*!@brief It shows whether Patient is satisfied or not*/
@property (assign, nonatomic) BOOL isSatisfied;

/*!
 @brief It's a custom constructor.
 
 @param name - Patient's name.
        temperature - Patient's temperature
        doctor - Patient's doctor;
*/
- (id) initWithName:(NSString*) name hasTemperature:(CGFloat) temperature hasDoctor:(ASDoctor*) doctor;
/*! @brief This function define whether Patient feels bad or not
    
    @return Returns a boolean value*/
- (BOOL) feelBad;
/*! @brief Patient takes a pill if he fills bad*/
- (void) takePill;
/*! @brief Patient make a shot if he fills worse*/
- (void) takeShot;
/*! @brief Patient changes a doctor, if he is not satisfied after a visit to the first one*/
- (void) changeDoctor:(NSArray*) doctors;

@end

/*!
 @protocol ASPatientDelegate
 
 @brief The ASPatientDelegate protocol
 
 @discussion This protocols explain to a doctor on how to act with Patients,and has the most important method - patientFeelsBad.
    Also has a gettter, to get doctors name from ASPatient class, and one enum with different body parts or organs, so the Patient would be able to explain, what exactly disturbing him, and doctor would be able to act appropriately.
 */
@protocol ASPatientDelegate

/*!
 @typedef BodyParts
 
 @brief  A struct about body parts or organs, that can disturb Patient.
 */
typedef enum {
    Stomac,
    Head,
    Leg,
    Heart,
}BodyParts;

/*! @brief It's getter for doctors name, wich can be called from Patient class*/
- (NSString*) getName;
/*! @brief Main method. If Patient fills bad, he would go to a doctor, and doctor will act appropriately
    
    @param patient - The reference to the Patient
 */
- (void) patientFeelsBad:(ASPatient*) patient;
/*! @brief Method converts an instance of enum BodyParts into string and return it
 
    @param bodypart - The instance of enum BodyParts.
 */
- (NSString*) getBodyPart:(BodyParts) bodypart;

@end