//
//  Patient.m
//  HomeTaskLessons
//
//  Created by Fantom on 2/5/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import "ASPatient.h"
#import "ASDoctor.h"

@implementation ASPatient

- (id) initWithName:(NSString*) name hasTemperature:(CGFloat) temperature hasDoctor:(ASDoctor *)doctor
{
    self = [super init];
    if (self) {
        _name = name;
        _temperature = temperature;
        _delegate = doctor;
        
        BOOL hasCough = arc4random() % 2;
        hasCough ? (_hasCough = TRUE) : (_hasCough = FALSE);
    }
    return self;
}

- (BOOL) feelBad
{
    BOOL reallyFeelBad = arc4random() % 2;
    
    if (reallyFeelBad) {
        [self.delegate patientFeelsBad:self];
    }
    return reallyFeelBad;
}

- (void) takePill
{
    NSLog(@"%@ takes a pill", _name);
}

- (void) takeShot
{
    NSLog(@"%@ takes a shot", _name);
}

- (void) changeDoctor:(NSArray*) doctors
{
    if (_delegate) {
        for (int i = 0, flag = 0; i < [doctors count] && flag == 0; i++){
            if ((ASDoctor*)_delegate != doctors[i]) {
                _delegate = doctors[i];
                flag += 1;
            }
        }
    }
}

@end

