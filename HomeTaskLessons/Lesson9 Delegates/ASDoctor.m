//
//  Doctor.m
//  HomeTaskLessons
//
//  Created by Fantom on 2/5/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import "ASDoctor.h"

@implementation ASDoctor

- (id) initWithName:(NSString*) name
{
    self = [super init];
    if (self) {
        _name = name;
        _patientsWithHeadAche = [NSMutableArray new];
        _patientsWithStomacAche = [NSMutableArray new];
        _patientsWithHeartAche = [NSMutableArray new];
        _patientsWithPainInLeg = [NSMutableArray new];
    }
    
    return self;
}

- (NSString*) getName
{
    return _name;
}

- (void) patientFeelsBad:(ASPatient*) patient
{
    BodyParts bodyPart = arc4random() % 4;
    
    NSLog(@"%@ comes to a %@. My %@ is paining. ", patient.name, _name, [self getBodyPart:bodyPart]);
    
    if (bodyPart == Stomac) {
        NSLog(@"%@ says: Take a pill to cure stomac ache", _name);
        [_patientsWithStomacAche addObject:patient];
    } else if (bodyPart == Head) {
        NSLog(@"%@ says: You need to sleep more than 5 hours", _name);
        [_patientsWithHeadAche addObject:patient];
    } else if (bodyPart == Leg) {
        NSLog(@"%@ says: Use this ointment to release a pain", _name);
        [_patientsWithPainInLeg addObject:patient];
    } else if (bodyPart == Heart) {
        NSLog(@"%@ says: You need to drink less coffee", _name);
        [_patientsWithHeartAche addObject:patient];
    }
    
    if (patient.temperature >= 37.f && patient.temperature <= 38.f) {
        [patient takePill];
    } else if (patient.temperature > 38.f) {
        [patient takeShot];
    } else if (patient.temperature > 38.f && patient.hasCough){
        [patient takePill];
        [patient takeShot];
    } else {
        NSLog(@"%@ just need to take rest", patient.name);
    }
    
    patient.isSatisfied = arc4random() % 2;
}

- (NSString*) getBodyPart:(BodyParts) bodypart
{
    if (bodypart == Stomac) {
        return @"Stomac";
    } else if (bodypart == Head) {
        return @"Head";
    } else if (bodypart == Leg) {
        return @"Leg";
    } else if (bodypart == Heart) {
        return @"Heart";
    }
    return @"There is no matches";
}

- (void) showAllPatientsOfTheDay
{
    NSLog(@"Today %@ had %d patients with stomac ache:", _name, (int)[_patientsWithStomacAche count]);
    for (ASPatient* patient in _patientsWithStomacAche) {
        NSLog(@"%@",patient.name);
    }
    
    NSLog(@"Today %@ had %d patients with head ache:", _name, (int)[_patientsWithHeadAche count]);
    for (ASPatient* patient in _patientsWithHeadAche) {
        NSLog(@"%@",patient.name);
    }
    
    NSLog(@"Today %@ had %d patients with heart ache:", _name, (int)[_patientsWithHeartAche count]);
    for (ASPatient* patient in _patientsWithHeartAche) {
        NSLog(@"%@",patient.name);
    }
    
    NSLog(@"Today %@ had %d patients with pain in the leg:", _name, (int)[_patientsWithPainInLeg count]);
    for (ASPatient* patient in _patientsWithPainInLeg) {
        NSLog(@"%@",patient.name);
    }
}



@end
