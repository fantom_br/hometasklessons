/*!
 @header ASDoctor.h
 
 @brief This is the header file of my ASDoctor class
 This class was created in order to accomplish home task of lesson9 Delegates, in IOS development Course in VK social network.
 
 @author Fantom
 @copyright  2015 Fantom
 @version    1.0
 */

#import <Foundation/Foundation.h>
#import "ASPatient.h"

/*!
 @class ASDoctor
 
 @brief The ASDoctor class
 
 @discussion This class is describing a doctor, which is implementing an ASPatientDelegate protocol. Doctor has his own book, where all patients accepted during the day were putted into this book, and divided by symptoms.
 @coclass: ASPatient and ASFriendOFDoctor
 */
@interface ASDoctor : NSObject <ASPatientDelegate>

/*! @brief Doctor's name */
@property (strong, nonatomic) NSString* name;
/*! @brief Book with patients with head ache */
@property (strong, nonatomic) NSMutableArray* patientsWithHeadAche;
/*! @brief Book with patients with stomac ache */
@property (strong, nonatomic) NSMutableArray* patientsWithStomacAche;
/*! @brief Book with patients with heart ache */
@property (strong, nonatomic) NSMutableArray* patientsWithHeartAche;
/*! @brief Book with patients with pain in the leg */
@property (strong, nonatomic) NSMutableArray* patientsWithPainInLeg;

- (id) initWithName:(NSString*) name;
/*! @brief Shows all patients accepted during the day divided by symptoms */
- (void) showAllPatientsOfTheDay;

@end
