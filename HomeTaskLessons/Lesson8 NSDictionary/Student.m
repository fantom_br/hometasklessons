//
//  NSDictionary.m
//  HomeTaskLessons
//
//  Created by Fantom on 2/4/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

@implementation Student

- (id)initWithName:(NSString *)name hisLastName:(NSString *)lastname
{
    self = [super init];
    if (self) {
        _name = name;
        _lastName = lastname;
    }
    return self;
}

- (void)sayHello
{
    NSLog(@"Hello, my name is %@!", _name);
}
@end