//
//  main.m
//  HomeTaskLessons
//
//  Created by Fantom on 2/4/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

int main() {
    @autoreleasepool {
        Student* student1 = [[Student alloc] initWithName:@"Vasia" hisLastName:@"Pupkin"];
        Student* student2 = [[Student alloc] initWithName:@"Kolia" hisLastName:@"Petrov"];
        Student* student3 = [[Student alloc] initWithName:@"Alex" hisLastName:@"Gruwa"];
        Student* student4 = [[Student alloc] initWithName:@"Devochka" hisLastName:@"Olya"];
        Student* student5 = [[Student alloc] initWithName:@"Ira" hisLastName:@"Kruglaia"];
        Student* student6 = [[Student alloc] initWithName:@"Ivan" hisLastName:@"Bublik"];
        Student* student7 = [[Student alloc] initWithName:@"Katia" hisLastName:@"Smirnova"];
        Student* student8 = [[Student alloc] initWithName:@"Evgenii" hisLastName:@"Tsarev"];
        Student* student9 = [[Student alloc] initWithName:@"Iva" hisLastName:@"Kozak"];
        Student* student10 = [[Student alloc] initWithName:@"Nester" hisLastName:@"Ivanovich"];
        
        NSDictionary* schoolBook = @{
                [[student1.name stringByAppendingString:@" "] stringByAppendingString: student1.lastName]:student1,
                [[student2.name stringByAppendingString:@" "] stringByAppendingString: student2.lastName]:student2,
                [[student3.name stringByAppendingString:@" "] stringByAppendingString: student3.lastName]:student3,
                [[student4.name stringByAppendingString:@" "] stringByAppendingString: student4.lastName]:student4,
                [[student5.name stringByAppendingString:@" "] stringByAppendingString: student5.lastName]:student5,
                [[student6.name stringByAppendingString:@" "] stringByAppendingString: student6.lastName]:student6,
                [[student7.name stringByAppendingString:@" "] stringByAppendingString: student7.lastName]:student7,
                [[student8.name stringByAppendingString:@" "] stringByAppendingString: student8.lastName]:student8,
                [[student9.name stringByAppendingString:@" "] stringByAppendingString: student9.lastName]:student9,
                [[student10.name stringByAppendingString:@" "] stringByAppendingString: student10.lastName]:student10,
                };
        
        //level Begginer: Printing each student
        NSLog(@"%@", schoolBook);
        
        //level Student: Printing each student with their welcoming message
        for (NSString* key in [schoolBook allKeys]) {
            Student* student = [schoolBook objectForKey:key];
            
            NSLog(@"%@ %@", student.name, student.lastName);
            [student sayHello];
        }
        
        //level Master: Sorting the array of dictionary keys in ascending order. After, printing welcoming message from sorted dictionary
        NSSortDescriptor* keySorting = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
        NSArray* sortedKeys = [[schoolBook allKeys] sortedArrayUsingDescriptors:@[keySorting]];
        
        for (NSString* key in sortedKeys) {
            Student* student = [schoolBook objectForKey:key];
            
            NSLog(@"%@ %@", student.name, student.lastName);
            [student sayHello];
        }
    }
    
    return 0;
}
