/*!
 @header Student.h
 
 @brief This is the header file of my Student class
 This class was created in order to accomplish home task of lesson8 Dictionary, in IOS development Course in VK social network.
 
 @author Fantom
 @copyright  2015 Fantom
 @version    1.0
 */

#ifndef HomeTaskLessons_Header_h
#define HomeTaskLessons_Header_h

/*!
 @class Student
 
 @brief The Student class
 
 @discussion This class have only 2 properties to define name and last name of student. Also, there is a one method to introduce himself.
 */
@interface Student : NSObject

/*! @brief This property define student's name. */
@property (nonatomic, copy) NSString* name;
/*! @brief This property define student's last name. */
@property (nonatomic, copy) NSString* lastName;

- (id)initWithName:(NSString*)name hisLastName:(NSString*)lastname;

/*! @brief It just say Hello. */
- (void)sayHello;

@end

#endif
