//
//  Lesson1.m
//  HomeTaskLessons
//
//  Created by Fantom on 2/1/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//
#ifndef LESSON_1
#define LESSON_1

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface Lesson6 : NSObject

- (void)action;

@end


@implementation Lesson6

- (id)init {
    self = [super init];
    if (self) {
        [self action];
    }
    return self;
}

- (void)action
{
    CGPoint point;
    CGRect rect = CGRectMake(3, 3, 3, 3);
    
    for (int i = 0; i < 100; i++) {
        point.x = arc4random() % 9;
        point.y = arc4random() % 9;
        
        bool containPoint = CGRectContainsPoint(rect, point);
        
        if (containPoint) {
            NSLog(@"%@", NSStringFromPoint(point));
        }
    }
}
@end


#endif //LESSON_1