#ifndef CYCLIST_H
#define CYCLIST_H

#import <Foundation/Foundation.h>
#import "Human.h"
#import "Runners.h"

@interface Cyclist: Human <Runners>

- (void)move;

@end

#endif // CYCLIST_H