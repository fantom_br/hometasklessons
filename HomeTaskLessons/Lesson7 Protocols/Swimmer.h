#ifndef SWIMMER_H
#define SWIMMER_H

#import <Foundation/Foundation.h>
#import "Human.h"
#import "Swimmers.h"

@interface Swimmer: Human <Swimmers>

- (void)move;

@end

#endif // SWIMMER_H