//
//  Runners.h
//  HomeTaskLessons
//
//  Created by Fantom on 2/2/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Runners <NSObject>

@optional
@property (strong, nonatomic) NSString* category;
@property (assign, nonatomic) NSInteger medalsQuantity;

@required
- (void)run;

@optional
- (void)doNothing;

@end
