//
//  Jumpers.h
//  HomeTaskLessons
//
//  Created by Fantom on 2/2/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Jumpers <NSObject>

@property (assign, nonatomic) NSInteger maxHeighOfJump;

- (void)jump;

@optional
- (void)relax;

@end
