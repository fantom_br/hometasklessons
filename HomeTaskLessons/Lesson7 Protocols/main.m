//
//  main.m
//  HumanTask
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Runners.h"
#import "Swimmers.h"
#import "Jumpers.h"
#import "Human.h"
#import "Swimmer.h"
#import "Cyclist.h"
#import "Runner.h"
#import "Skier.h"
#import "Dog.h"
#import "Cat.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Human* human = [Human new];
        Swimmer* swimmer = [Swimmer new];
        Runner* runner = [Runner new];
        Cyclist* cyclist = [Cyclist new];
        Skier* skier = [Skier new];
        Dog* dog = [Dog new];
        Cat* cat = [Cat new];
        
        NSArray* people = @[swimmer, runner, cyclist, skier];
        NSArray* animals = @[dog, cat];
        NSArray* entities = [animals arrayByAddingObjectsFromArray:people];

        
        for (id <Jumpers, Swimmers, Runners> entity in entities) {
            if ([entity conformsToProtocol:@protocol(Jumpers)]) {
                [entity jump];
                
                if ([entity respondsToSelector:@selector(relax)]) {
                    [entity relax];
                }
            }
            if ([entity conformsToProtocol:@protocol(Swimmers)]) {
                [entity swim];
            
                if ([entity respondsToSelector:@selector(dive)]) {
                    [entity dive];
                }
            }
            if ([entity conformsToProtocol:@protocol(Runners)]) {
                [entity run];
                
                if ([entity respondsToSelector:@selector(doNothing)]) {
                    [entity doNothing];
                }
            }
        }
    }
    
    return 0;
}
