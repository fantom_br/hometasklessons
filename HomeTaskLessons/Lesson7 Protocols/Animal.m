//
//  Animal.m
//  TaskHuman
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Animal.h"

@implementation Animal

- (id)init
{
    self = [self initWithValues:@"Alfred" paws:4];
    return self;
}

- (id)initWithValues:(NSString*)nickName paws:(int)pawsQuantity
{
    self = [super init];
    if (self) {
        _type = @"Animal";
        _nickName = nickName;
        _pawQuantity = pawsQuantity;
    }
    return self;
}

- (void)move
{
    NSLog(@"Animal is running!");
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"Type: %@\n Nickname: %@\n Has paws: %d", _type, _nickName, _pawQuantity];
}

@end