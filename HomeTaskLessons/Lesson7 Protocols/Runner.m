#import "Runner.h"

@implementation Runner

@synthesize category = _category;
@synthesize medalsQuantity = _medalsQuantity;

- (id)init
{
    self = [super initWithValues:@"Runner" hisGender:@"Female" hisHeight:155 hisWeight:100];
    _category = @"master of sport";
    _medalsQuantity = 45;
    
    return self;
}

- (void)move
{
    [super move];
    NSLog(@"Runner is running!");
}

- (void)run
{
    NSLog(@"I'm Runner, and I refuse to run.\n I'm running as %@ for 25 year.\n I have %d medals as runner", _category, (int)_medalsQuantity);
}

- (void)doNothing
{
    NSLog(@"Leave me along, I'm just chiling out.");
}

@end