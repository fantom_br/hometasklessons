//
//  Cat.m
//  TaskHuman
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cat.h"

@implementation Cat

@synthesize swimmingSpeed = _swimmingSpeed;

- (id)init
{
    self = [super initWithValues:@"Cat" paws:4];
    _swimmingSpeed = 0;
    
    
    return self;
}

- (void)move
{
    NSLog(@"Cat is too lazy to move!");
}

- (void)swim
{
    NSLog(@"Cat doesn't like swimming at all... His max speed is %d km/h.", (int)_swimmingSpeed);
}

@end