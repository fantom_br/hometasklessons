#import "Human.h"

@implementation Human

- (id)init
{
    self = [self initWithValues:@"Human"
        hisGender:@"Male"
        hisHeight:170
        hisWeight:80];
    
    return self;
}

- (id)initWithValues: (NSString*)name
    hisGender: (NSString*)gender
    hisHeight: (int)height
    hisWeight: (int)weight
{
    self = [super init];
    if (self) {
        _type = @"Human";
        _name = name;
        _gender = gender;
        _height = height;
        _weight = weight;
    }
    return self;
}

- (void)move
{
    NSLog(@"Human is moving!");
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"Type: %@\n Name: %@\n %@'s gender: %@\n %@'s height: %d\n %@'s weight: %d", _type, _name, _name, _gender, _name, _height, _name, _weight];
}

@end