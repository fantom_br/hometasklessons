#import "Skier.h"

@implementation Skier

@synthesize maxHeighOfJump = _maxHeighOfJump;
@synthesize swimmingSpeed = _swimmingSpeed;
@synthesize medalsQuantity = _medalsQuantity;

- (id)init
{
    self = [self initWithValues:@"Skier"
                        hisGender:@"Male"
                        hisHeight:170
                        hisWeight:80
                        hisEquipment:@"Ski"
                        hisAge:20
                        hisIsProfessional:TRUE];
    return self;
}

- (id)initWithValues: (NSString*)name
    hisGender: (NSString*)gender
    hisHeight: (int)height
    hisWeight: (int)weight
    hisEquipment: (NSString*)equipment
    hisAge: (int)age
    hisIsProfessional: (BOOL)isProfessional
{
    self = [super initWithValues:name 
        hisGender:gender
        hisHeight:height
        hisWeight:weight];
    
    if (self)
    {
        _equipment = equipment;
        _age = age;
        _isProfessional = TRUE;
        _medalsQuantity = 0;
        _maxHeighOfJump = 3;
        _swimmingSpeed = 14;
    }
    return self;
}

- (void)move
{
    [super move];
    NSLog(@"Skier is skiing!");
}

- (NSString*)description
{   
    NSString* descriptionOfSuperClass = [super description];
    NSString* descriptionOfthisClass = [NSString stringWithFormat:@"\n %@'s age: %d\n %@ is professional: %hhd\n %@'s equipment: %@", self.name, _age, self.name,  _isProfessional, self.name, _equipment];
    
    return [descriptionOfSuperClass stringByAppendingString:descriptionOfthisClass];
}

- (void)jump
{
    NSLog(@"Skier can jump like Bubka. The max heigh of jump is %d meters.", (int)_maxHeighOfJump);
}

- (void)swim
{
    NSLog(@"Skier can swim. The max swimming speed is %d km/h.", (int)_swimmingSpeed);
}

- (void)run
{
    NSLog(@"Skier can run. As skier is not professional, he has %d medals as sportsman.", (int)_medalsQuantity);
}

@end

