//
//  Animal.h
//  TaskHuman
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#ifndef TaskHuman_Animal_h
#define TaskHuman_Animal_h

@interface Animal: NSObject

@property (nonatomic, readonly) NSString* type;
@property (nonatomic, readonly) NSString* nickName;
@property (nonatomic, readonly) int pawQuantity;

- (id)initWithValues: (NSString*)nickName paws:(int)pawsQuantity;
- (void)move;
- (NSString*)description;

@end

#endif
