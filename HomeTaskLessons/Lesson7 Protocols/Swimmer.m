#import "Swimmer.h"

@implementation Swimmer

@synthesize swimmingSpeed = _swimmingSpeed;

- (id)init
{
    self = [super initWithValues:@"Swimmer" hisGender:@"Male" hisHeight:190 hisWeight:60];
    _swimmingSpeed = 16;
    
    return self;
}

- (void)move
{   
    [super move];
    NSLog(@"Swimmer is swimming!");
}

- (void)swim
{
    NSLog(@"Suddenly, swimmer is swimming... His max speed is %d km/h.", (int)_swimmingSpeed);
}

- (void)dive
{
    NSLog(@"Swimmer can dive sometimes");
}

@end