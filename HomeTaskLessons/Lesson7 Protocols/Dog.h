//
//  Dog.h
//  TaskHuman
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#ifndef TaskHuman_Dog_h
#define TaskHuman_Dog_h

#import "Animal.h"
#import "Jumpers.h"
#import "Runners.h"

@interface Dog : Animal <Jumpers, Runners>

- (void)move;

@end

#endif
