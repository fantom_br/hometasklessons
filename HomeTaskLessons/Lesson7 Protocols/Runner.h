#ifndef RUNNER_H
#define RUNNER_H

#import <Foundation/Foundation.h>
#import "Human.h"
#import "Runners.h"

@interface Runner: Human <Runners>

- (void)move;

@end

#endif //RUNNER_H