#import "Cyclist.h"

@implementation Cyclist

@synthesize category = _category;
@synthesize medalsQuantity = _medalsQuantity;

- (id)init
{
    self = [super initWithValues:@"Cyclist" hisGender:@"Male" hisHeight:160 hisWeight:60];
    _category = @"not professional";
    _medalsQuantity = 0;
    
    return self;
    
}

- (void)move
{
    [super move];
    NSLog(@"Cyclist is cycling!");
}

- (void)run
{
    NSLog(@"I'm Cyclist, and I'm running after my bike.\n I'm running as %@.\n I have %d medals as runner", self.category, (int)self.medalsQuantity);
}

@end