//
//  Dog.m
//  TaskHuman
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dog.h"

@implementation Dog

@synthesize maxHeighOfJump = _maxHeighOfJump;
@synthesize category = _category;
@synthesize medalsQuantity = _medalsQuantity;

- (id)init
{
    self = [super initWithValues:@"Dog" paws:4];
    _maxHeighOfJump = 3;
    _category = @"dog's master of sport";
    _medalsQuantity = 5;
    
    return self;
}

- (void)move
{
    NSLog(@"Dog is running with his master!");
}

- (void)jump
{
    NSLog(@"Dog is crazy about jumping!");
}

- (void)run
{
    NSLog(@"Dog is running like crazy.\n Dog running as %@.\n He has %d medals as runner", self.category, (int)self.medalsQuantity);
}

- (void)relax
{
    NSLog(@"Dog is just chilling!");
}

@end