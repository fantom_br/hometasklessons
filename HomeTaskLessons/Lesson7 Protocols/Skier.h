#ifndef SKIER_H
#define SKIER_H

#import <Foundation/Foundation.h>
#import "Human.h"

@interface Skier: Human <Jumpers, Swimmers, Runners>

@property(nonatomic, readonly, copy) NSString* equipment;
@property(nonatomic, readonly) int age;
@property(nonatomic, readonly) BOOL isProfessional;

- (id)initWithValues: (NSString*)name
    hisGender: (NSString*)gender
    hisHeight: (int)height
    hisWeight: (int)weight
    hisEquipment: (NSString*)equipment
    hisAge: (int)age
    hisIsProfessional: (BOOL)isProfessional;

- (void)move;
- (NSString*)description;

@end

#endif // SKIER_H