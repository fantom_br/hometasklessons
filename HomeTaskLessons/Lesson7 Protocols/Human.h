#ifndef HUMAN_H
#define HUMAN_H

#import <Foundation/Foundation.h>
#import "Jumpers.h"
#import "Swimmers.h"
#import "Runners.h"

@interface Human: NSObject 

@property (nonatomic, readonly) NSString* type;
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSString* gender;
@property (nonatomic, readonly) int height;
@property (nonatomic, readonly) int weight;

- (id)initWithValues: (NSString*)name
    hisGender: (NSString*)gender
    hisHeight: (int)height
    hisWeight: (int)weight;

- (void)move;
- (NSString*)description;

@end

#endif //HUMAN_H