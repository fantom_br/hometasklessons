//
//  Cat.h
//  TaskHuman
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#ifndef TaskHuman_Cat_h
#define TaskHuman_Cat_h

#import "Animal.h"
#import "Swimmers.h"

@interface Cat : Animal <Swimmers>

- (void)move;

@end

#endif
